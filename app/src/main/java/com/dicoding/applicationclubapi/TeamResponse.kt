package com.dicoding.applicationclubapi

/**
 * Created by developer on 9/17/18.
 */
data class TeamResponse(
        val teams: List<Team>)