package com.dicoding.applicationclubapi

import android.view.View

/**
 * Created by developer on 9/17/18.
 */
fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}
