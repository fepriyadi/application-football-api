package com.dicoding.applicationclubapi

import java.net.URL

/**
 * Created by developer on 9/17/18.
 */
class ApiRepository {

    fun doRequest(url: String): String {
        return URL(url).readText()
    }
}