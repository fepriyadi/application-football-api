package com.dicoding.applicationclubapi

/**
 * Created by developer on 9/17/18.
 */
interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<Team>)
}