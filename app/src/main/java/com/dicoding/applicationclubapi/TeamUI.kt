package com.dicoding.applicationclubapi

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import org.jetbrains.anko.*

/**
 * Created by developer on 9/17/18.
 */
class TeamUI : AnkoComponent<ViewGroup> {

    companion object {
        var teamBadge = R.id.team_badge
        var teamName = R.id.team_name
    }
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                padding = dip(16)
                orientation = LinearLayout.HORIZONTAL

                imageView {
                    id = teamBadge
                }.lparams{
                    height = dip(50)
                    width = dip(50)
                }

                textView {
                    id = teamName
                    textSize = 16f
                }.lparams{
                    margin = dip(15)
                }

            }
        }
    }

}